import Chat from './src/components/chat/Chat'
import rootReducer from './src/reducers/rootReducer'

const bsa = {
    Chat,
    rootReducer
}

export default bsa
