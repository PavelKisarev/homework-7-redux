import React from 'react';
import './index.css'

export default function Modal() {
    return (
        <div className="edit-message-modal">
            <div className="edit-message-container">
                <textarea className="edit-message-input form-control"></textarea>
                <div className="edit-message-group">
                    <button className="btn btn-primary edit-message-button">Update</button>
                    <button className="btn btn-secondary edit-message-close">Close</button>
                </div>
            </div>
        </div>
    )
}
