import React from 'react';
import './index.css';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import { ADD_NEM_MESSAGE } from '../../reducers/constants';

export default function MessageInput(props) {

    let [message, setMessage] = React.useState('');

    const dispatch = useDispatch()


    const messageHandler = (e) => {
        setMessage(e.target.value)
    }

    const clickHandler = () => {
        dispatch({
            type: ADD_NEM_MESSAGE,
            payload: {
                id: uuidv4(),
                text: message,
                createdAt: moment().format('hh:mm'),
                own: true
            }
        })
        setMessage('')
    }

    return (
        <div className="message-input">
            <textarea onInput={messageHandler} value={message} className="message-input-text form-control" name="" ></textarea>
            <button onClick={() => { clickHandler() }} className="message-input-button btn btn-primary">Send</button>
        </div>
    )
}
