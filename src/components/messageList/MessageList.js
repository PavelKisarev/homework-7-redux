import React from 'react';
import './index.css';
import Message from '../message/Message';
import OwnMessage from '../ownMessage/OwnMessage';

export default function MessageList(props) {

    let { messageArr } = props;

    messageArr = messageArr.map((el, i) => {
        if (el?.own) {
            return <OwnMessage key={el.id} messageElem={el} />
        }
        else {
            return <Message key={el.id} messageElem={el} />
        }
    })

    return (
        <ul className="message-list">
            {messageArr}
        </ul>

    )
}
