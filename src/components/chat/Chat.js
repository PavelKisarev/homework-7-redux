import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import moment from 'moment';
import Header from '../header/Header';
import MessageList from '../messageList/MessageList';
import MessageInput from '../messageInput/MessageInput';
import Preloader from '../preloader/Preloader';
import Modal from '../modal/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_MESSAGES } from '../../reducers/constants';

export default function Chat({ url }) {
    const [checkLoad, SetCheckLoad] = React.useState(false)

    const dispatch = useDispatch();
    let dataChat = useSelector(state => state.chat);
    let isLoading = dataChat.preloader;

    React.useEffect(() => {
        fetch(url).then(res => res.json()).then(res => {

            dispatch({
                type: LOAD_MESSAGES,
                payload: {
                    messages: res,
                    preloader: false
                }
            })

            SetCheckLoad(true)
        });
    }, [url, dispatch])

    React.useEffect(() => {
        SetCheckLoad(false)
    }, [dataChat])


    let countAllMessages = dataChat.messages.length

    let countAllUsers = new Set(dataChat.messages.map((el) => {
        return el.userId
    })).size

    let timeLastMessage = moment(dataChat.messages[dataChat.messages.length - 1]?.createdAt).format('hh:mm')


    return (
        <div className="chat">
            <div className="container-fluid bg-dark bg-gradient ">
                <div className="row">
                    <div className="col-12">
                        <Header
                            allMessages={countAllMessages}
                            allUsers={countAllUsers}
                            timeLastMessage={timeLastMessage}
                        />
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-8">
                        {isLoading ? <Preloader /> : <MessageList messageArr={dataChat.messages} />}
                        <MessageInput />
                    </div>
                </div>
            </div>

            <Modal />
        </div>

    )
}
