import React from 'react';
import './index.css';
import moment from 'moment';

export default function Message(props) {

    // console.log(props);

    let { avatar, createdAt, user, text } = props.messageElem


    return (
        <li className="message">
            <img className="message-user-avatar" src={avatar} alt="" />
            <span className="message-content">
                <span className="message-user-name">{user} <span className="message-time">{moment(createdAt).format('hh:mm')}</span></span>
                <span className="message-text">{text}</span>
                <button className="message-like btn btn-warning">
                    Like
                </button>
            </span>
        </li>
    )
}
