import React from 'react';
import './index.css';
import { useDispatch } from 'react-redux';
import { DELETE_MESSAGE } from '../../reducers/constants'

export default function OwnMessage(props) {
    const { id, createdAt, text } = props.messageElem;

    const dispatch = useDispatch()

    const deleteHandler = () => {
        // props.deleteF(id)
        dispatch({
            type: DELETE_MESSAGE,
            payload: id
        })
    }

    return (
        <li className="own-message">
            <span className="message-content">
                <span className="message-time">{createdAt}</span>
                <span className="message-text">{text}</span>
                <span className="own-btn-group">
                    <button className="message-edit btn btn-info">
                        Edit
                    </button>
                    <button className="message-delete btn btn-danger" onClick={() => { deleteHandler() }}>
                        Delete
                    </button>
                </span>
            </span>
        </li>
    )
}
