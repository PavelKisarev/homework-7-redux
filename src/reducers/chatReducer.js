import { LOAD_MESSAGES, ADD_NEM_MESSAGE, DELETE_MESSAGE } from '../reducers/constants'

const chatReducer = (state = [], action) => {
    switch (action.type) {
        case LOAD_MESSAGES: {
            const messages = action.payload.messages;
            const preloader = action.payload.preloader;
            state.messages = [...messages];
            state.preloader = preloader;
            return state
        }
        case ADD_NEM_MESSAGE: {
            const newMsg = action.payload;
            state.messages.push(newMsg)
            console.log(state)
            return state
        }
        case DELETE_MESSAGE: {
            const delID = action.payload;
            let updatedMessagesArr = state.messages.filter((el) => el.id !== delID);
            state.messages = updatedMessagesArr
            console.log(state.messages)
            return state
        }
        default: return state
    }
}

export default chatReducer