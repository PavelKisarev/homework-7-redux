import { createStore } from 'redux';
import rootReducer from './reducers/rootReducer';

const initState = {
    chat: {
        messages: [],
        editModal: false,
        preloader: true
    }
}

let store = createStore(rootReducer, initState)

export default store